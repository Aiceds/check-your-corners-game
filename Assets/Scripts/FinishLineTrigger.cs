﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLineTrigger : MonoBehaviour
{
    GameObject WorldTimerObject;

    void Start()
    {
        WorldTimerObject = GameObject.FindGameObjectWithTag("World");
    }

    void Update()
    {
        //if (endTrig == true)
        //{
        //    Debug.Log("timer stopped");
        //    //make timer stop here
        //    //GameObject.FindGameObjectWithTag("StartingLine").GetComponent<StartingLineTrigger>().timer = 0;
        //    WorldTimerObject.GetComponent<StartingLineTrigger>().timer = 0;
        //}
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            WorldTimerObject.GetComponent<TimerScript>().timerStart = false;
        }
    }
}
