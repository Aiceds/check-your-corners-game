﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public ParticleSystem muzzleFlash; //Premade particle animation

    public GameObject hitAudio;
    public AudioClip[] MetalAudio;
    public AudioClip WallAudio;
    public AudioClip[] Gunshot;
    public AudioClip[] MetalHeadshotAudio;

    //int shotNumber = 0
    ////Invoke("thefunctiontocall", 2.0f);

    private void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1")) //On Left Mouse Click
        {
            int randomNumber = Random.Range(0, 4);

            hitAudio.GetComponent<AudioSource>().PlayOneShot(Gunshot[randomNumber]);

            muzzleFlash.Play(); //Play premade particle animation

            RaycastHit hit; //Raycast collision
            float distanceOfRay = 100; //Max range of raycast

            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, distanceOfRay)) //If raycast shoots out of center of main camera
            {
                hitAudio.transform.position = hit.point;

                if (hit.transform.gameObject.CompareTag("Target Head")) //If a target got shot in the head
                {
                    //Destroy(hit.transform.parent.gameObject);
                    hit.transform.parent.gameObject.SetActive(false); //Disable pearented gameObject
                    ScoreScript.scoreValue += 2; // Adds a value of 2 to the score value in the score script

                    Invoke("playMetalHeadshotImpactAudio", 0.03f);
                }
                //Debug.Log(hit.transform.name);
                if (hit.transform.gameObject.CompareTag("Target Body")) //If a target got shot in the body
                {
                    //Destroy(hit.transform.parent.gameObject);
                    hit.transform.parent.gameObject.SetActive(false); //Disable pearented gameObject
                    ScoreScript.scoreValue += 1; // Adds a value of 1 to the score value in the score script

                    Invoke("playMetalImpactAudio", 0.03f);
                }

                if (hit.transform.gameObject.CompareTag("Environment"))
                {
                    hitAudio.GetComponent<AudioSource>().PlayOneShot(WallAudio);
                }

            }
            Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * distanceOfRay); //Debug to draw a ray within the editor of Unity
        }

        if (Input.GetButtonDown("Fire2")) //On Right Mouse Click Down
        {
            Time.timeScale = 0.2f; //Add slow motion
        }

        if (Input.GetButtonUp("Fire2")) //On Right Mouse Click Up
        {
            Time.timeScale = 1f; //Change back to normal time
        }
    }

    void playMetalImpactAudio()
    {
        int randomNumber = Random.Range(0, 4);

        hitAudio.GetComponent<AudioSource>().PlayOneShot(MetalAudio[randomNumber]);
    }

    void playMetalHeadshotImpactAudio()
    {
        int randomNumber = Random.Range(0, 4);

        hitAudio.GetComponent<AudioSource>().PlayOneShot(MetalHeadshotAudio[randomNumber]);
    }
}
