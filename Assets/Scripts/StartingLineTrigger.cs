﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingLineTrigger : MonoBehaviour
{
    GameObject WorldTimerObject;

    // Start is called before the first frame update
    void Start()
    {
        WorldTimerObject = GameObject.FindGameObjectWithTag("World");
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            WorldTimerObject.GetComponent<TimerScript>().timerStart = true;
        }
    }
}
