﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController: MonoBehaviour
{
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    private Vector3 moveDirection = Vector3.zero;
    private float SideX;
    private float VertY;
    public float sensitivity;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CharacterController controller = GetComponent<CharacterController>();
        // is the controller on the ground?
        if (controller.isGrounded)
        {
            //Feed moveDirection with input.
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            //Multiply it by speed.
            moveDirection *= speed;
            //Jumping
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;
        }

        SideX = Input.GetAxis("Mouse X") * sensitivity;
        VertY = -Input.GetAxis("Mouse Y") * sensitivity;


        if (SideX != 0)
        {
            //Code for action on mouse moving right
            transform.eulerAngles += new Vector3(0, SideX, 0);
        }
       
        if (VertY != 0)
        {
   
            //Code for action on mouse moving right
            transform.eulerAngles += new Vector3(VertY, 0, 0);
        }

        //if(transform.eulerAngles.x < -80.0f)
        //{
        //    transform.eulerAngles = new Vector3(-80.0f, transform.eulerAngles.y, transform.eulerAngles.z);
        //}

        //Applying gravity to the controller
        moveDirection.y -= gravity * Time.deltaTime;
        //Making the character move
        controller.Move(moveDirection * Time.deltaTime);
    }
}
