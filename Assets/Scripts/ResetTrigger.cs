﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetTrigger : MonoBehaviour
{
    GameObject targetValueReset;

    // Start is called before the first frame update
    void Start()
    {
        targetValueReset = GameObject.FindGameObjectWithTag("Reset");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //Resets Targets
        GameObject[] allRooms = GameObject.FindGameObjectsWithTag("Room");

        if (other.gameObject.CompareTag("Player"))
        {
            // Resets Target Randomisation
            for (int i = 0; i < allRooms.Length; i++)
            {
                allRooms[i].GetComponent<ActiveTargets>().setTargetsActive();
            }

            //targetValueReset.GetComponent<ScoreScript>().scoreReset = true;
            GameObject.FindGameObjectWithTag("Score").GetComponent<ScoreScript>().resetScore();
        }
    }
}
