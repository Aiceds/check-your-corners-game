﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    public static int scoreValue = 0;
    Text scoreText;

    public static int activeTargetValue = 0;
    private GameObject[] targetSearch;

    public bool scoreReset = false;

    // Start is called before the first frame update
    void Start()
    {
        resetScore();
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score: " + scoreValue + "/" + activeTargetValue;

        //if (scoreReset == true)
        //{
        //    scoreText = GetComponent<Text>();
        //    activeTargetValue = 0;
        //    scoreValue = 0;
        //    targetSearch = GameObject.FindGameObjectsWithTag("Target");

        //    foreach (GameObject target in targetSearch)
        //    {
        //        if (target.activeSelf)
        //        {
        //            activeTargetValue += 2;
        //        }
        //    }
        //}
    }

    public void resetScore()
    {
        scoreText = GetComponent<Text>();
        activeTargetValue = 0;
        scoreValue = 0;
        targetSearch = GameObject.FindGameObjectsWithTag("Target");

        foreach (GameObject target in targetSearch)
        {
            if (target.activeSelf)
            {
                activeTargetValue += 2;
            }
        }
    }
}
