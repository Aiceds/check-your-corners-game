﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveTargets : MonoBehaviour
{
    public GameObject[] listOfTargets; //gameObject array of targets
    public int chanceAmount; //Set number can be changed in unity

    // Start is called before the first frame update
    void Awake() // When the script instance is being loaded
    {
        setTargetsActive(); //Call to the function to set the targets active
    }

    public void setTargetsActive() //The function to set the targets active
    {
        //for (int i = 0; i < listOfTargets.Length; i++)
        //{
        //    int randomNumb = Random.Range(0, 10);
        //    if(randomNumb < chanceAmount)
        //    {
        //        listOfTargets[i].SetActive(true);
        //    }
        //}
        // listOfTargets[Random.Range(0, listOfTargets.Length)].SetActive(true);

        for (int i = 0; i < listOfTargets.Length; i++) //For each entity in array
        {
            int randomNumb = Random.Range(0, 10); //Select random number from 1 to 10
          
            if (randomNumb < chanceAmount) //If the random number is less then the set number
            {
                listOfTargets[i].SetActive(true); //Activate object
            }
            else
            {
                listOfTargets[i].SetActive(false); //Deactivate object
            }
        }
    }
}