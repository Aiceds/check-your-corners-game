﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
    public float timer = 0f;
    public Text timerTextBox;

    public bool timerStart = false;
    public bool timerReset = false;

    // Start is called before the first frame update
    void Start()
    {
        timerTextBox.text = FormatTime(timer);
    }

    // Update is called once per frame
    void Update()
    {
        if (timerStart == true)
        {
            timerTextBox.text = FormatTime(timer);
            //timerTextBox.text = timer.ToString("f2");
            timer += Time.deltaTime;
        }
        else if (timerStart == false)
        {
            timer = 0f;
        }
    }

    //This function takes the timer float and reformats the string into time format
    public string FormatTime(float time)
    {
        int minutes = (int)time / 60;
        int seconds = (int)time - 60 * minutes;
        int milliseconds = (int)(100 * (time - minutes * 60 - seconds));
        return string.Format("{0:00}:{1:00}:{2:00}", minutes, seconds, milliseconds);
    }
}
