﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unbreakable : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
    }
}
